# CTAO ESAP SDC Portal

Development of a portal for the CTAO Science Data Challenge (SDC), based on ESAP GUI Gateway (started as a fork).

## Architecture

Django web project based on ESAP API and its collection of applications, with the addition of a client application for direct rendering dedicated to the CTAO SDC.

## Development Guidelines

* The ESAP API applications should remain similar to the upstream versions, so as to keep up to date with the latest ESAP API developments.
* The sdc_portal application should contain all pages dedicated to the SDC portal, and connect to the ESAP API for internal processing when necessary.
* The branch from_uptream contains the main branch of the upstream project ESAP API Gateway, so as to merge future ESAP developments.
* The main branch should only receive pull requests for the SDC Portal development.
* Commits should be pushed to development branches dedicated to a feature or modification.

## Installation for development

See ESAP API dev mode:
https://git.astron.nl/astron-sdc/escape-wp5/esap-api-gateway/-/wikis/Build-&-Deploy/Development-mode-(localhost)




# ESAP API Gateway
ESAP API Gateway is a 'backend' web application written in Django.
It provides a range of services that can be accessed through a REST API.

## Documentation

* https://git.astron.nl/astron-sdc/esap-api-gateway/-/wikis/home

## Contributing

For developer access to this repository, please send a message on the [ESAP channel on Rocket Chat](https://chat.escape2020.de/channel/esap).
