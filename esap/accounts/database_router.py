from esap.database_router import EsapBaseRouter

class AccountsRouter(EsapBaseRouter):
    app_to_db = {'accounts': 'accounts'}
