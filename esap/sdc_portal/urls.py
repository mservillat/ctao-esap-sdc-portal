from django.urls import path

from . import views

app_name = "sdc_portal"

urlpatterns = [
    path("", views.home, name="home"),
    path("signin", views.signin, name="signin"),
    path("logout", views.logout, name="logout"),
    path("search", views.search, name="search"),
]
