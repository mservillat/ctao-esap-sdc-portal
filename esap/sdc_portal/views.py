from django.contrib import messages
from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse
from django.shortcuts import render, redirect, reverse

import logging
import json

from django.utils.http import urlencode

from .forms import DemoForm
from .fieldstore import FieldStore
from .tap import Tap, convert_astropy_table_to_array

logger = logging.getLogger('query')

def home(request):
    return render(request, "sdc_portal/home.html")


def signin(request):
    return redirect(reverse('oidc_authentication_init'))


def logout(request):
    return redirect(reverse('oidc_logout'))


def search(request):
    '''
    basic search interface
    '''

    logger.info('search view')

    json_dt_data = ''
    json_dt_cols = ''

    store = FieldStore()

    fields = store.get_fields()

    # init fields to their default values
    for field in fields.values():
        field['value'] = field['default']

    logger.info(fields)


    # To work the same way with GET or POST
    args=None
    if request.method == 'POST': # If form was posted
        args=request.POST

    if request.method == 'GET':
        args=request.GET

    logger.info('args={}'.format(args))

    if len(args) != 0:  # some parameters where passed


        # Create a form from the posted data
        demo_form = DemoForm(args)

        if demo_form.is_valid():

            logger.info('demo_form is valid')

            logger.info(demo_form.cleaned_data)

            # set the value of the field according to what's found in the cleaned form
            for k in fields.keys():
                fields[k]['value'] = demo_form.cleaned_data[k]

            if True:

                # (args.get('submit')):  # Button submit was clicked to post the form

                logger.info("Submit button was clicked")

                tap_error, res_table = perform_query(fields)


                if tap_error == None:

                    colnames = res_table.fieldnames
                    logger.info(colnames)

                    # convert res_table (pyvo table) into an array
                    logger.info('Converting pyvo table to json string')
                    rows = convert_astropy_table_to_array(res_table)



                    #nb_rows = len(rows)


                    # prepare data for datatables
                    dt_data = rows

                    dt_cols = []
                    for c in colnames:
                        dt_cols.append({"title": c})



                    json_dt_data = json.dumps(dt_data, cls=DjangoJSONEncoder)
                    json_dt_cols = json.dumps(dt_cols, cls=DjangoJSONEncoder)

                else:
                    messages.error(request,tap_error)


        else:  # form is not valid
            logger.info(demo_form.errors.as_data())
            messages.error(request, 'Error(s) in form: {} Please correct form'.format(demo_form.errors.as_data()))

    # Create form for params, fill it with initial values or values got from POSTed form
    # NB: some field's values be None as number coming from cleaned_data => change None to ''
    initial = {}
    for k in fields.keys():
        initial[k] = '' if fields[k]['value'] is None else fields[k]['value']
    logger.info(initial)

    demo_form = DemoForm(
        auto_id=True,
        initial=initial,
    )


    context = {
        'demo_form': demo_form,
        'json_dt_data': json_dt_data,
        'json_dt_cols': json_dt_cols,
    }

    #logger.info('context={}'.format(context))

    return render(request, 'sdc_portal/search.html', context)



def perform_query(fields):
    '''
    Perform TAP query and returns TAP error (None if OK) and result table
    '''

    logger.info(fields)

    #TODO: move config to dedicated settings variables

    # URL of TAP server
    url='http://voparis-tap-he.obspm.fr/tap'

    # Name of obscore table on the TAP server
    obscore_table = 'hess_dr.obscore_sdc'

    # timeout for TAP server connection
    timeout = 5


    t = Tap(url)

    logger.info('Connecting, timeout={} secs'.format(timeout))
    t.connect(timeout)



    # construct ADQL query - here a conesearch
    query = ("SELECT TOP 100 * FROM {} WHERE 1=CONTAINS(POINT('ICRS',s_ra,s_dec),CIRCLE('ICRS',{},{},{}))".format(
       obscore_table,
       fields['target_raj2000']['value'],
       fields['target_dej2000']['value'],
       fields['search_radius']['value']))



    logger.info('Sending query={} to url={} with timeout={}'.format( query, url, timeout))
    exception, res_table = t.query(query)

    if (exception == None):

        logger.info('No exception, got res_table with {} rows'.format(len(res_table)))

        #print(res_table)

        error = None
    else:
        # try to get exception name
        exception_s = str(exception)
        if exception_s == '':  # happens with TimeoutException
            exception_s = type(exception).__name__

        error = 'Got exception with TAP query: {}'.format(exception_s)
        logger.info('TAP Error: {}'.format( error))


    return error, res_table
