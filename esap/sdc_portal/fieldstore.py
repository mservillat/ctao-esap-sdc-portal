'''
Created on 2023-09-14
Class to hold fields
@author: renaud.savalle@obspm.fr
'''

class FieldStore(object):

    fields = {}

    def __init__(self):

        self.fields = {
                'target_raj2000':       {'default': None},
                'target_dej2000':       {'default': None},
                'search_radius':        {'default': None},
        }

    def get_fields(self):
        return self.fields




