'''
Created on 2023-09-14
Class to perform TAP query and convert results
Current implementation uses pyvo
@author: renaud.savalle@obspm.fr
'''

# from astroquery.utils.tap import TapPlus as atap
import pyvo as vo

import math
import numpy as np

# import timeout_decorator
# import stopit # not used anymore, timeout handled in class MyHTTPAdapter

import requests



class MyHTTPAdapter(requests.adapters.HTTPAdapter):
    '''
    a subclass of HTTPAdapter to handle timeouts
    cf https://github.com/psf/requests/issues/3341#issuecomment-226764983
    '''

    def set_timeout(self, timeout):
        self.timeout = timeout

    def send(self, *args, **kwargs):
        kwargs['timeout'] = self.timeout
        return super(MyHTTPAdapter, self).send(*args, **kwargs)


class Tap(object):
    '''
    class to handle TAP/ADQL queries
    '''
    url = None
    conn = None

    def __init__(self, url):
        '''
        Constructor

        Args:
            url (str): url of the TAP endpoint
        '''
        self.url = url

    def connect(self, timeout=5):
        '''
        Retrieve a connection to the TAP server, setting a timeout for the connection
        '''

        # astroquery implementation - deprecated
        # self.conn = atap(self.url)

        # Get a requests session to pass it to TAPService ctor
        session = requests.Session()

        # Change session to use MyHTTPAdapter which has a timeout - cf https://github.com/psf/requests/blob/d2590ee46c0641958b6d4792a206bd5171cb247d/requests/sessions.py#L414
        adapter = MyHTTPAdapter()
        adapter.set_timeout(timeout)

        session.mount('https://', adapter)
        session.mount('http://', adapter)

        self.conn = vo.dal.TAPService(self.url, session)

    def query(self, query):
        '''
        Launch a TAP query

        Note:
            With the astroquery implementation:
            "TOP 2000" will be added to the query by astroquery.utils.tap,
            so to get more results, add a TOP n (n>2000) in the query

        Note:
            After the call, check the exception first, if None, proceed
            with the table


        Args:
            query (str): ADQL query

        Returns:
            - exception: Exception caught during TAP query or None
            - table (astropy.table.table.Table): table with results
        '''

        table = None
        exception = None

        try:
            # Perform query and retrieve results
            # NB: HTTP errors are caught as exceptions

            # Implementation with astroquery.utils.tap, DEPRECATED
            # job = self.conn.launch_job(query=query, verbose=True)
            # table = job.get_results()
            # socket.setdefaulttimeout(5) # attempt to timeout... does not work

            # Implementation with vo.dal.TAPService
            table = self.conn.search(query)

            # For astroquery implem: per email from juan.carlos.segovia@sciops.esa.int 2019-07-01 this should work, but does not
            # (always return 0 and None):
            if False:
                http_status = job.responseStatus  # job.get_response_status()
                http_msg = job.responseMsg  # job.get_response_msg()
                print("tap.query: HTTP status: {} HTTP msg: {}".format(http_status, http_msg))

        except Exception as e:
            print(
                "Tap.query to {}: {} Exception: {}. (pyvo version={})".format(self.url, type(e), e, vo.version.version))
            exception = e

        # res = {
        #     'table': table,
        #     'exception': exception,
        # }

        return exception, table



def convert_astropy_table_to_array(table):
    """
    Convert a astropy table to array of array then suitable for JSON conversion
    """

    # logger.info('convert_astropy_table_to_array')

    rows = []
    for r in range(len(table)):
        # logger.info('Row {}'.format(r))
        res_row = table[r]
        row = []
        for c in table.fieldnames:
            # logger.info('Col {}'.format(c))
            cell = res_row[c]
            # logger.info(type(res_cell))

            if isinstance(cell, (bytes, np.bytes_)):  # 2020-03-05 added numpy.bytes_ for CADC
                # if type(res_cell) in (bytes, numpy.bytes_):
                cell = str(cell.decode('utf-8'))

            if isinstance(cell, (int, np.int64)):  # 2020-03-05 added numpy.int64 for CADC
                cell = int(cell)
            else:
                try:
                    cell = float(cell)
                    if math.isnan(cell):
                        cell = None
                except ValueError:
                    pass
            row.append(cell)
        # logger.info(row)
        rows.append(row)
    return rows

