from django.apps import AppConfig


class SdcPortalConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sdc_portal'
