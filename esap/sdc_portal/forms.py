'''
Created on 2023-09-14
Class to define forms
@author: renaud.savalle@obspm.fr
'''

from django import forms


class DemoForm(forms.Form):

    target_raj2000_desc = 'Right Ascension of target in deg (J2000)'
    target_raj2000 = forms.DecimalField(
        label='Target RA (deg):', required = True,
        help_text=target_raj2000_desc,
        widget=forms.NumberInput(attrs={
                                      'placeholder': target_raj2000_desc,
                                      'class': 'form-control form-control-sm',
                                      }),
        min_value=0.0, max_value=360.0,
    )

    target_dej2000_desc = 'Declination of target in deg (J2000)'
    target_dej2000 = forms.DecimalField(
        label='Target DEC (deg):', required = True,
        help_text='Declination of target in deg (J2000)',
        widget=forms.NumberInput(attrs={
                                      'placeholder': target_dej2000_desc,
                                      'class': 'form-control form-control-sm',
                                      }),
        min_value=(-90.0), max_value=90.0,
    )

    search_radius_desc = 'Cone Search radius in deg.'
    search_radius = forms.DecimalField(
        label='Cone Search Radius (deg):', required = True,
        help_text=search_radius_desc,
        widget=forms.NumberInput(attrs={
                                        'placeholder': search_radius_desc,
                                        'class': 'form-control form-control-sm',
                                        }),
        min_value=0.0, max_value=90.0,
    )
