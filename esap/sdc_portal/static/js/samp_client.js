var samp_client = ( function($) {
	"use strict";

	var jss = {}, metadata = {
		"samp.name": "CTAstro",
		"samp.description": "CTAstro",
		"samp.icon.url": "https://portal.cta-observatory.org/Pictures/cta_logo_blue_small.png",
		"author.name": "",
		"author.affiliation": "Observatoire de Paris, LUTH",

	},
	connector = new samp.Connector("CTAstro", metadata, null),/* connection = null,*/ msgs;

	function connHandler(conn) {
		var i;
		for ( i = 0; i < msgs.length; i = i + 1) {
			conn.notifyAll([msgs[i]]);
		}
	}

	function regErrorHandler(e) {
		alert("SAMP error, please check if a SAMP hub is running");
	}

	function send() {
		connector.runWithConnection(connHandler, regErrorHandler);
	}

	function unregister() {
		if (connector.connection) {
			connector.unregister();
		}
	}

	function samp_votable(votable_url, votable_name) {
		var msg = new samp.Message("table.load.votable", {
//			"table-id" : votable_id,
			"url" : votable_url,
			"name" : votable_name
		});
		msgs = [msg];
		send();
	}

	function samp_image(url) {
		var msg = new samp.Message("image.load.fits", {
			"url" : url
		});
		msgs = [msg];
		send();
	}

	function samp_spectrum(url) {
		var msg = new samp.Message("spectrum.load.ssa-generic", {
			//"meta" : {"Access.Format":access_format},
			"url" : url
		});
		msgs = [msg];
		send();
	}

	function samp_data(data){
		var i,msg;
		msgs = [];
		for (i=0;i<data.length;i=i+1){
			if (data[i].type === 'image'){
				msg = new samp.Message("image.load.fits", {
					"url" : data[i].access_url
//					"name" : "TEST"
				});
				msgs.push(msg);
			}
			else if (data[i].type === 'spectrum'){
				msg = new samp.Message("spectrum.load.ssa-generic", {
					"url" : data[i].access_url
//					"name" :
				});
				msgs.push(msg);
			}
			else if (data[i].type === 'votable'){
				msg = new samp.Message("table.load.votable", {
					"url" : data[i].access_url
//					"name" :
				});
				msgs.push(msg);
			}
			else if (data[i].type === 'fitstable'){
				msg = new samp.Message("table.load.fits", {
					"url" : data[i].access_url
//					"name" :
				});
				msgs.push(msg);
			}
		}
		send();
	}

	$(window).on("unload", function(e) {
        unregister();
    });

	/* Exports. */
	jss.samp_votable = samp_votable;
	jss.samp_image = samp_image;
	jss.samp_spectrum = samp_spectrum;
	jss.samp_data = samp_data;
	jss.unregister = unregister;

	return jss;
})(jQuery);

/*
To send data via SAMP, build a data list as follows:

			var i, data_list = [];
			if (data.length != 0) {
				for ( i = 0; i < data.length; i++) {
					var url = data[i]['access_url'];
					var data_item = {};
					data_item['access_url'] = url;
					data_item['type'] = 'fitstable';  // or votable, image, spectrum...
					data_list.push(data_item);
					logger('INFO', 'Data sent to SAMP hub: '+url);
				}
				samp_client.samp_data(data_list);
			}
*/