simbad = document.getElementById("simbad_button");
simbad.onclick = resolveQuery;

//simbad_list = document.getElementById("simbad_list");
simbad_result = document.getElementById("resolve_result");

input_ra = document.getElementById("id_ra");
input_dec = document.getElementById("id_dec");

function getCookie(name) {
	let cookieValue = null;
	if (document.cookie && document.cookie !== '') {
		const cookies = document.cookie.split(';');
		for (let i = 0; i < cookies.length; i++) {
			const cookie = cookies[i].trim();
			// Does this cookie string begin with the name we want?
			if (cookie.substring(0, name.length + 1) === (name + '=')) {
				cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
				break;
			}
		}
	}
	return cookieValue;
}
const csrftoken = getCookie('csrftoken');

function resolveQuery() {
	var name = document.getElementById("simbad_input").value;
	simbad_result.innerHTML = '';
	var result = document.createTextNode("Loading...");
	simbad_result.appendChild(result);
	console.log(name);

	const requestOptions = {
		method: 'POST',
		credentials: 'include',
		mode: 'same-origin',
		headers: {
			'Accept': 'application/json',
			'Content-type': 'application/json',
			'X-CSRFToken': csrftoken,},
		body: JSON.stringify({
			data: name,
		})
	};

	console.log(requestOptions);

	fetch('/api/sim_res/', requestOptions)
		.then(res => res.json())
		.then(res => updateList(res));
}

function updateList(data) {
	/*console.log(data);
	simbad_list.innerHTML = '';*/
	simbad_result.innerHTML = '';
	for(i=0; i<data.length; i++) {
		/*var newOption = document.createElement("option");
		var text = document.createTextNode(data[i].Main_identifier);
		newOption.appendChild(text);
		simbad_list.appendChild(newOption);*/
		input_ra.value = data[i].ra;
		input_dec.value = data[i].dec;

		var result = document.createTextNode("We've found this identifier : " + data[i].Main_identifier);
		simbad_result.appendChild(result);
	}
}