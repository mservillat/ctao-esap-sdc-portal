(function($) {
    "use strict";

    $(document).ready(function() {

        $('button[data-bs-toggle="tab"]').on('shown.bs.tab', function(e){
           $($.fn.dataTable.tables(true)).DataTable()
              .columns.adjust();
        });

   		if(dataQS[0] != null) {
			var columnNamesQS = Object.keys(dataQS[0]);

			for (var i in columnNamesQS) {
				columnsQS.push({title: columnNamesQS[i],
				data: columnNamesQS[i]});
			}

            $('#query_store').DataTable( {
                "dom": 'Blfrtip',
                scrollY: "600px",
                scrollX: true,
                scrollCollapse: true,
                select: {
                    style: 'single',
                },
                buttons: [
                    {
                        extend: 'selected',
                        text: 'Execute Query',
                        action: function(e, dt, type, indexes) {
                            var req_id = dt.rows( { selected: true } ).data().toArray()[0]["id"];
                            window.location.replace("/results/"+ req_id);
                        }
                    },
                ],
                responsive: true,
                data: dataQS,
                columns: columnsQS,
            });
		}

		if(data[0] != null) {
			var columnNames = Object.keys(data[0]);

			for (var i in columnNames) {
				var col = columnNames[i];
				if(['obs_id', 'target_name', 'obs_publisher_did', 'quality', 'target_offset', 'safe_energy_lo', 'safe_energy_hi'].indexOf(col)>=0) {
					columns.push({
					    title: col,
					    data: col,
					});
				}

				else {
					columns.push({
					    title: col,
					    data: col,
					    visible: false,
					});
				}
			}

            $('#results_table').DataTable( {
                "dom": 'QBlfrtip',
                scrollY: "600px",
                scrollX: true,
                scrollCollapse: true,
                select: true,
                buttons: [
                    'colvis',
                    {
                        extend: 'selected',
                        text: 'Aladin Preview',
                        action: function(e, dt, button, config) {
                            var coordinates = [];
                            var selection = dt.rows( { selected: true } ).data();
                            console.log(selection.length);
                            for(i=0; i<selection.length; i++) {
                                coordinates.push({
                                    ra: selection[i].s_ra,
                                    dec: selection[i].s_dec,
                                    ra_obj: selection[i].ra_obj,
                                    dec_obj: selection[i].dec_obj,
                                    target_name: selection[i].target_name,
                                    obs_id: selection[i].obs_id,
                                });
                            }
                            console.table(coordinates);
                            createNewTab(coordinates);
                        }
                    },
                    {
                        extend: 'selected',
                        text: 'Send to SAMP',
                        action: function(e, dt, button, config) {
                            var access_urls = [];
                            var selection = dt.rows( { selected: true } ).data();
                            console.log(selection.length);
                            for(i=0; i<selection.length; i++) {
                                access_urls[i] = {access_url: selection[i].access_url};
                            }

                            var i, data_list = [];
                            if (access_urls.length != 0) {
                                for ( i = 0; i < access_urls.length; i++) {
                                    var url = access_urls[i]['access_url'];
                                    var data_item = {};
                                    data_item['access_url'] = url;
                                    data_item['type'] = 'fitstable';  // or votable, image, spectrum...
                                    data_list.push(data_item);
                                    //logger('INFO', 'Data sent to SAMP hub: '+url);
                                }
                                samp_client.samp_data(data_list);
                            }
                        }
                    },
                    'selectAll',
                    'selectNone',
                ],
                responsive: true,
                data: data,
                columns: columns,
            });

			function createNewTab(coordinates) {
				var nextTab = $('#tabs li').length+1;

				var id_tab = 'aladin'+nextTab;
				var body = "<div id='"+id_tab+"' style='width:700px;height:600px;'></div>";

				// create the tab
				//$('<li><a href="#tab'+nextTab+'" data-toggle="tab">Tab '+nextTab+'</a></li>').appendTo('#tabs');
				$("<li><button class='nav-link' id='"+nextTab+"_tab' data-bs-toggle='tab' data-bs-target='#tab"+nextTab+"' type='button' role='tab' aria-controls='"+nextTab+"' aria-selected='true'> Aladin #"+nextTab+"</button></li>").appendTo('#tabs');

				// create the tab content
				$('<div class="tab-pane fade" id="tab'+nextTab+'" role="tabpanel" aria-labelledby="'+nextTab+'_tab">'+body+'</div>').appendTo('#results_navigation');

				var aladin = A.aladin("#"+id_tab+"", {survey: "P/Fermi/color", fov:60, target: ""+coordinates[0].ra_obj+" "+coordinates[0].dec_obj+"", cooFrame: "ICRSd"});

				var cat = A.catalog({name: 'Observations', sourceSize: 15});
				var cat_t = A.catalog({name: 'Targets', sourceSize: 15, color: 'red'});
				aladin.addCatalog(cat);
				aladin.addCatalog(cat_t);
				for(i=0; i<coordinates.length; i++) {
					cat.addSources(A.marker(coordinates[i].ra, coordinates[i].dec, {popupTitle: coordinates[i].obs_id}));
					cat_t.addSources(A.marker(coordinates[i].ra_obj, coordinates[i].dec_obj, {popupTitle: coordinates[i].target_name}));
					console.log(coordinates[i]);
				}

				// make the new tab active
				$('#tabs button:last').tab('show');
			}
		}

    });

})(jQuery);
