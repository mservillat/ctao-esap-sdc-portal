from esap.database_router import EsapBaseRouter

class BatchRouter(EsapBaseRouter):
    app_to_db = {'batch': 'batch'}
