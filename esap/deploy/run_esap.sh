#!/bin/bash
# ESAP 'production' run script

echo "=== ESAP Installation script 2 of 2 ==="

export ESAP_ROOT=${ESAP_ROOT:-$HOME/esap}
export ESAP_SRC=$ESAP_ROOT/src
export ESAP_SHARED=$ESAP_ROOT/shared
export ESAP_GUI_DIR=$ESAP_SRC/esap-gui

# test for modern docker compose command
docker compose version
if [ "$?" -eq "0" ]; then
  DOCKER_COMPOSE="docker compose"
else
  DOCKER_COMPOSE="docker-compose"
fi

cd $ESAP_GUI_DIR

mkdir -p $ESAP_SHARED/public_html
mkdir -p $ESAP_SHARED/public_html/esap-gui
mkdir -p $ESAP_SHARED/static

echo "building esap-gui frontend in $ESAP_GUI_DIR"
# Test for modern nodejs
NODE_MAJOR_VERSION=$(node --version | grep -oP "v\K([[:digit:]]+)")
if [ "$NODE_MAJOR_VERSION" -gt 16 ]; then
  NODE_OPTIONS=--openssl-legacy-provider npm run build
else
  npm run build
fi


cp -r $ESAP_GUI_DIR/build/*.* $ESAP_SHARED/public_html/esap-gui
cp -r $ESAP_GUI_DIR/build/static/* $ESAP_SHARED/static

# start esap
$DOCKER_COMPOSE -f $ESAP_SHARED/docker-compose.yml up --build -d

# copy the generated databases in the container to the shared location (do not override)
if [ ! -f $ESAP_SHARED/esap_ida_config.sqlite3 ]
then
  docker exec -it esap_api_gateway cp esap/esap_ida_config.sqlite3 /shared/esap_ida_config.sqlite3
fi

if [ ! -f $ESAP_SHARED/esap_batch_config.sqlite3 ]
then
  docker exec -it esap_api_gateway cp esap/esap_batch_config.sqlite3 /shared/esap_batch_config.sqlite3
fi

if [ ! -f $ESAP_SHARED/esap_rucio_config.sqlite3 ]
then
  docker exec -it esap_api_gateway cp esap/esap_rucio_config.sqlite3 /shared/esap_rucio_config.sqlite3
fi

if [ ! -f $ESAP_SHARED/esap_uws.sqlite3 ]
then
  docker exec -it esap_api_gateway cp esap/esap_uws.sqlite3 /shared/esap_uws.sqlite3
fi

if [ ! -f $ESAP_SHARED/esap_accounts_config.sqlite3 ]
then
  docker exec -it esap_api_gateway cp esap/esap_accounts_config.sqlite3 /shared/esap_accounts_config.sqlite3
fi

if [ ! -f $ESAP_SHARED/esap_config.sqlite3 ]
then
  docker exec -it esap_api_gateway cp esap/esap_config.sqlite3 /shared/esap_config.sqlite3
fi

echo "=== ESAP Installation PART 2 done ==="
