#!/bin/bash
# ESAP 'production' install script

echo "=== ESAP Installation script 1 of 2 ==="

export ESAP_ROOT=${ESAP_ROOT:-$HOME/esap}
export ESAP_SRC=$ESAP_ROOT/src
export ESAP_SHARED=$ESAP_ROOT/shared

# create work directory and shared directory
mkdir -p $ESAP_SRC
mkdir -p $ESAP_SHARED
mkdir -p $ESAP_SHARED/public_html

# get backend code
echo "downloading esap-api backend..."
cd $ESAP_SRC
export ESAP_API_DIR=$ESAP_SRC/esap-api-gateway/esap

if [ ! -d $ESAP_API_DIR ]
then
   echo "git clone repository..."
   git clone https://git.astron.nl/astron-sdc/esap-api-gateway.git
else
   echo "git pull repository..."
   cd esap-api-gateway
   git pull
fi

# build it
echo "building esap-api backend..."
cd $ESAP_API_DIR
docker build -t esap_api_gateway:latest .

# copy files to shared directory
echo "copy shared files..."
if [ ! -f $ESAP_SHARED/esap_config.sqlite3 ]
then
  cp $ESAP_API_DIR/esap/*.sqlite3 $ESAP_SHARED
fi

if [ ! -f $ESAP_SHARED/oidc.env ]
then
  cp $ESAP_API_DIR/deploy/shared/oidc.env $ESAP_SHARED
fi

if [ ! -f $ESAP_SHARED/esap.env ]
then
  cp $ESAP_API_DIR/deploy/shared/esap.env $ESAP_SHARED
fi

if [ ! -f $ESAP_SHARED/nginx.conf ]
then
  cp $ESAP_API_DIR/deploy/shared/nginx.conf $ESAP_SHARED
fi

if [ ! -f $ESAP_SHARED/docker-compose.yml ]
then
  cp $ESAP_API_DIR/deploy/shared/docker-compose.yml $ESAP_SHARED
fi

if [ ! -f $ESAP_SHARED/Dockerfile ]
then
  cp $ESAP_API_DIR/deploy/shared/Dockerfile $ESAP_SHARED
fi

# get frontend code
echo "downloading esap-gui frontend..."
cd $ESAP_SRC
export ESAP_GUI_DIR=$ESAP_SRC/esap-gui

if [ ! -d $ESAP_GUI_DIR ]
then
   echo "git clone repository..."
   git clone https://git.astron.nl/astron-sdc/esap-gui.git
else
  echo "git pull repository..."
  cd esap-gui
  git pull
fi

echo "installing frontend dependencies..."
cd $ESAP_GUI_DIR
npm install


# Worker code
echo "downloading esap-worker..."
export ESAP_WORKER_DIR=$ESAP_SRC/esap-worker
cd $ESAP_SRC

if [ ! -d $ESAP_WORKER_DIR ]
then
   echo "git clone repository..."
   git clone https://git.astron.nl/astron-sdc/esap-worker.git
else
   echo "git pull repository..."
   cd esap-worker
   git pull
fi

# build it
echo "building esap-api backend..."
cd $ESAP_WORKER_DIR
docker build -t esap_worker:latest .

# edit the api_host for production
echo ""
echo "=== ESAP Installation PART 1 done ==="
echo "configure the frontend by pointing 'api_host' to <your_host>/esap-api/"
echo "  \$EDITOR $ESAP_GUI_DIR/src/contexts/GlobalContext.js"
echo ""
echo "configure the OIDC settings for your client"
echo "  \$EDITOR ~/esap_shared/oidc.env"
echo ""
echo "...then run the 'run_esap.sh' script."
