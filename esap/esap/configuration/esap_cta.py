#import requests
#import urllib.parse
#import json

title = "cta"
logo = "https://upload.wikimedia.org/wikipedia/commons/d/d1/Cherenkov_Telescope_Array_logo.jpg"

# definition of the query
query_schema = {
    "name": "cta",
    "title": "CTA Query",
    "type": "object",
    "properties": {
        "particle": {
            "type": "string",
            "title": "particle",
            "default": "proton",
            "enum": ["proton","gamma-diffuse"],
            "enumNames": ["Proton","Gamma-diffuse"]
        },
        #"keyword": {
        #    "type": "string",
        #    "title": "keyword",
        #},
        "catalog": {
            "type": "string",
            "title": "Catalog",
            "enum": ["cta"],
            "enumNames": ["Cta"]
        }
    }#,
    #"required": ["community"]
}
#ui_schema = {"keyword": {"ui:help": "e.g. CTA", "ui:placeholder": "optional"}, "catalog": {"ui:widget": "hidden"}}
ui_schema = {#"keyword": {"ui:help": "e.g. CTA"}, 
	      #"particle": {"ui:placeholder": "Proton"},
	      "catalog": {"ui:widget": "hidden"}}

